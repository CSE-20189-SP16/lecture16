Lecture 16 - Networking
=======================

Simple
------

1. `simple_echo_client.py`: Simple [TCP] echo [client].

2. `simple_echo_server.py`: Simple [TCP] echo [server].

**Note**: These simpler versions are provided to demonstrate the core [socket]
operations, but they do not utilize command-line arguments or do any error
checking.

OOP
---

1. `echo_client.py`: Object-oriented [TCP] echo [client].

2. `echo_server.py`: Object-oriented [TCP] echo [server].

**Note**: These are more complete versions of the simple implementations above
that include the use of classes, command-line arguments, logging, and error
checking.

[TCP]:	    https://en.wikipedia.org/wiki/Transmission_Control_Protocol
[client]:   https://en.wikipedia.org/wiki/Client_(computing)
[server]:   https://en.wikipedia.org/wiki/Server_(computing)
[socket]:   https://en.wikipedia.org/wiki/Network_socket
